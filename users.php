<?php 

    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
   
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Oscar Urbano">

<title>Orbis users</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">    

<style>

/* Full-width input fields */
/*input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}*/

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #FFF; /*color of the x of the close button*/
    opacity: 0.7;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

</style>

</head>

<body onload="load()">
 
<?php 
    if(isset($_SESSION['user_rol']) && $_SESSION['user_rol'] == 'admin'){
        include('nav_admin.html');
    }
    else{
        include('nav_user.html');
    }
    include('php/mainInclude.php');//conection to dataBase
?>

<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center" colspan="5">Users</th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <th class="text-center"> ID </td>
            <th class="text-center">Name</td>
            <th class="text-center">Rol</td>
            <th class="text-center">Last login</td>
            <td class="text-center">
            <?php
            if($_SESSION["user_rol"]=="admin"){
                echo '
                    <button class = "btn-primary btn-sm" onclick="onAdd()" style="width:auto;"> Add </button>
                ';
            }else{
                echo '
                    <button class = "btn-primary btn-sm" onclick="notAllowed()" style="width:auto;"> Add </button>
                ';
            }
            ?>
            </td>
        </tr>

        <?php        
        $result = $conn->query("SELECT * FROM Users WHERE Companies_idCompany = ".$_SESSION["company_id"]." ORDER BY idUser desc;");
        while($row = $result->fetch_array()){
            if ($row['idUser']>2  && ($_SESSION["user_rol"]=="admin") ){ //preventing from modifying initial users
                echo "
                        <tr>
                            <td>".$row['idUser']."</td>
                            <td>".$row['name']."</td>
                            <td>".$row['rol']."</td>
                            <td>".$row['lastLog']."</td>
                            <td class=\"text-center\">
                                <button class=\"btn-secondary btn-sm\" style=\"width:auto\" onclick= \"editUser(".$row['idUser'].")\" >
                                    Edit
                                </button> 
                            </td>
                        </tr>
                    ";
            }else{
                echo "
                        <tr>
                            <td>".$row['idUser']."</td>
                            <td>".$row['name']."</td>
                            <td>".$row['rol']."</td>
                            <td>".$row['lastLog']."</td>
                            <td class=\"text-center\">
                                <button class=\"btn-secondary btn-sm\" style=\"width:auto\" onclick= \"notAllowed()\" >
                                    Edit
                                </button> 
                            </td>
                        </tr>
                    ";                
            }
        } 
        ?>

<!--         <tr>
            <td>0000</td>
            <td><a href="#">Oscar Urbano</a></td>
            <td>Admin</td>
            <td>25/11/2017 13:00:00 </td>
            <td class="text-center">
                <button class = "btn-secondary btn-sm" onclick="document.getElementById('editUserDiv').style.display='block'" style="width:auto;">
                     Edit
                </button>
            </td>
        </tr> -->
    </tbody>
</table>    
</div>  
</div>  
</div>






<!-- popup modal form addUserMenu -->
<div  id="addUserDiv" class="modal" >
<form class="modal-content animate" action="php/usersManagement/addUser.php" method="POST">

<div class="imgcontainer" style="background-color: #336699; margin: 0px 0 0px 0; text-align: center; ">
    <img src="img/logov2.png" alt="orbis logo" style="height: 100px;">        
</div>

<span onclick="document.getElementById('addUserDiv').style.display='none'" class="close" title="Close Modal">&times; </span>

<div class="container-fluid">
    <fieldset class="form-group">
        <!-- <input id="addUserMenu_id" name ="userId" type="hidden"></input> -->
        <label for="user">User</label>
        <input id="addUserMenu_user" type="text" name ="user" class="form-control">
    </fieldset>
    <fieldset class="form-group">
        <label for="pass">Password</label>
        <input id="addUserMenu_pass" type="password" name="pass" class="form-control">
    </fieldset>
    <fieldset class="form-group">
        <select id="addUserMenu_rol" name="rol">
        <!-- <option selected disabled>Select a rol </option> -->
        <option value=2> user </option>
        <option value=1> admin </option>
        </select>
    </fieldset>
    <div class="row">
        <button type="button" class="btn btn-success center-block" onclick="addUser();">Add</button>
    </div>
    <br>
</div>

</form>
</div>






<!-- popup modal form editUserMenu -->
<div  id="editUserDiv" class="modal" >
<form class="modal-content animate" action="php/usersManagement/modifyUser.php" method="POST">

<div class="imgcontainer" style="background-color: #336699; margin: 0px 0 0px 0; text-align: center; ">
    <img src="img/logov2.png" alt="orbis logo" style="height: 100px;">        
</div>

<span onclick="document.getElementById('editUserDiv').style.display='none'" class="close" title="Close Modal">&times; </span>

<div class="container-fluid">
    <fieldset class="form-group">
        <input id="editUserMenu_id" name ="userId" type="hidden"></input>
        <label for="user">User</label>
        <input id="editUserMenu_user" type="text" name ="user" class="form-control" readonly>
    </fieldset>
<!--     <fieldset class="form-group"> it has no sense to change the password of a given user
        <label for="pass">Password</label>
        <input id="editUserMenu_pass" type="password" name="pass" class="form-control">
    </fieldset>
 -->    <fieldset class="form-group">
        <select id="editUserMenu_rol" name="rol" >
        <option selected disabled>Select a rol </option>
        <option value=1> admin </option>
        <option value=2> user </option>
        </select>
    </fieldset>
    <div class="row" style="text-align: center;">
        <div class="col-md-2">
            <!-- <br> -->
        </div>
        <div class="col-md-2" >
            <button type="submit" class="btn btn-success center-block">modify</button>
        </div>
        <div class="col-md-4">
            <br>
        </div>
        <div class="col-md-2">
            <button id="editUserMenu_removeButton" type="button" class="btn btn-danger center-block" onclick="removeUser();">remove</button>
        </div>
        <div class="col-md-2">
            <!-- <br> -->
        </div>
    </div>
    <br>
</div>

</form>
</div>
<!-- Example of popup form
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_login_form_modal -->





<script type="text/javascript"> 

function load() {
  


}

function onAdd(){
    document.getElementById('addUserDiv').style.display='block';
}

function notAllowed(){
    //alert("not allowed");
}


function addUser(){
    var user = $('#addUserMenu_user').val();
    var pass = $('#addUserMenu_pass').val();
    var rol = $('#addUserMenu_rol').val();
    //alert("user: "+user+", pass: "+pass+" , rol: "+rol);

    if ( user.length > 0  && pass.length > 0 ){

        $.ajax({
            method:"POST",
            url:"php/usersManagement/addUser.php",
            data: {"user":user,"pass":pass,"rol":rol}
        }).done(function(data){
            if(data.length>0){
                alert(data);
                window.location="users.php"
            }
            else{
                $('#addUserDiv').hide();
                window.location="users.php"
            }
        });

    }else{
        alert("user or pass are empty");
    }

}


function editUser(idUser){
    //alert("I'm gonna edit user: " +idUser);

    var user = '';
    var rol = '';

    //getting Data from DataBase with the php script
    $.ajax({
        method:"POST",
        url:"php/usersManagement/getDataUserFromId.php",
        data: {"userId":idUser}
    }).done(function(data){
        //console.log(data);
        var result = $.parseJSON(data);
        //console.log(result);
        user = result[0].name;
        rol = (result[0].rol=="admin")?  1:2;


    //Updating fields of the popUpMenu
        $(document.getElementById('editUserDiv').style.display='block').ready(function(){
            $('#editUserMenu_id').val(idUser);
            $('#editUserMenu_user').val(user);
            //$('#editUserMenu_pass').val(pass);//it gives the hash MD5 value. There's no way to get the real password
            $('#editUserMenu_rol').val(rol);
        });

    });


}

function removeUser(){
    var idUser = $('#editUserMenu_id').val();
    //alert("I'm gonna remove user: "+idUser);

    $.ajax({
        method:'POST',
        url:"php/usersManagement/removeUser.php",
        data:{"userId":idUser}
    }).done(function(data){
        if (data.length>0){
            alert(data);
        }else{
            $('#editUserDiv').hide();
            window.location="users.php";            
        }
    });
}


</script>
    
    
  

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>   
</html>
