<?php 
	session_start();
?>


<!DOCTYPE html>
<html lang="en">

<head>
   
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Oscar Urbano">

<title>show Registers</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">    

</head>

<body>
 
<?php 
    if(isset($_SESSION['user_rol']) && $_SESSION['user_rol'] == 'admin'){
        include('nav_admin.html');
    }
    else{
        include('nav_user.html');
    }
    include('php/mainInclude.php');//conection to dataBase
?>




<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<table class="table table-bordered table-striped table-hover">

	<thead>
		<tr>
			<th colspan="5" class="text-center">Registers</th>
		</tr>
	</thead>

	<tr>
		<th>ID value</th>
		<th>Value</th>
		<th>timeStamp</th>
		<th>DataSets_idDataSet</th>
		<th>Companies_idCompany</th>
	</tr>

	<tbody>
		<?php
		
		$result = $conn->query("SELECT * FROM ".$_CONF['db_db'].".Values ORDER BY idValue desc;");//When table empty: //Fatal error: Call to a member function fetch_array() on boolean in /opt/lampp/htdocs/smartTags/showRegisters.php on line 61

		while($row = $result->fetch_array()){
			echo "
					<tr>
						<td>".$row['idValue']."</td>
						<td>".$row['value']."</td>
						<td>".$row['timeStamp']."</td>
						<td>".$row['DataSets_idDataSet']."</td>
						<td>".$row['Companies_idCompany']."</td>
					</tr>
				";
		} 
		?>
	</tbody>
</table>
</div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 
</body>
</html>



