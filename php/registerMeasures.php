<?php

//TODO:
// Adding support for linear type of values. -> Check sizeof(ds_values) == sizeof(ds_timeStamps)
// Error handling when json null -> now it returns "error: Validating companyId"
include('mainInclude.php');

$param = $HTTP_RAW_POST_DATA;
$param =utf8_encode($param);
$json = json_decode($param,true);
echo json_encode($json, JSON_PRETTY_PRINT);//printing json 
echo json_last_error_msg()."\n";

//--- Getting data from json
$gateway = $json[0]["Gateway"];
$tag = $json[0]["Tag"];
$dataSet = $json[0]["DataSet"];
//gateway
$gw_Serial = $gateway["gatewaySerial"]; //it has to exist in the DB
//tag
$tag_TID = $tag["TID"]; //it has to exist in the DB
$tag_EPC = $tag["EPC"];
$tag_timeStamp = $tag["timeStamp"];//when data was read from the smartTag
//dataSet
$ds_sensorType = $dataSet["sensorType"];
$ds_logType = $dataSet["logType"];
$ds_ttLog = $dataSet["ttLog"]; //when >0 each value was logged every ttLog minutes
$ds_timeStamp = $dataSet["timeStamp"];
$ds_values = $dataSet["values"];
$ds_timeStamps = $dataSet["timeStamps"];
#print_r($ds_values);

//--- checking if gateway exist in DB
$queryReq = "SELECT * FROM Gateways WHERE serial = '".$gw_Serial."'";
#echo $queryReq."\n";
$result = $conn->query($queryReq);
#echo $conn->error . "\n";
$infoItem = $result->fetch_array();
$gatewayId = $infoItem['idGateway'];
$gatewayCompanyId = $infoItem['Companies_idCompany'];

//--- checking if tag exists in DB
$queryReq = "SELECT * FROM Tags WHERE TID = '".$tag_TID."'";
#echo $queryReq."\n";
$result = $conn->query($queryReq);
#echo $conn->error . "\n";
$infoItem = $result->fetch_array();
$tagId = $infoItem['idTag'];
$tagCompanyId = $infoItem['Companies_idCompany'];

//---checking companyId
$companyId = 0;
if( $gatewayCompanyId == $tagCompanyId){
	$companyId = $tagCompanyId;
}


if($companyId){// tag and gateway belong to the same company
if( $gatewayId ){ // Gateway exists

	//---updating gateway lastLog
	$result = $conn->query("UPDATE Gateways SET lastLog = '".date("Y-m-d H:i:s",time())."' WHERE idGateway = '".$gatewayId."';");
	if ($conn->error) echo $conn->error . "\n";

	if( $tagId ){ // Tag exists

		//---updating Tag lastRead and lastGatewayID
		$result = $conn->query("UPDATE Tags SET lastRead = '".$tag_timeStamp."' , Gateways_idGateway = '".$gatewayId."' WHERE idTag = '".$tagId."';");
		if ($conn->error) echo $conn->error . "\n";

		//---inserting DataSet table
		$stmt = $conn->prepare("INSERT INTO DataSets (sensorType, logType, ttLog, timeStamp, Tags_idTag, Gateways_idGateway, Companies_idCompany ) VALUES ( ?, ?, ?, ?, ?, ?, ? )");
		$stmt->bind_param("sssssss", $ds_sensorType, $ds_logType, $ds_ttLog, $ds_timeStamp, $tagId, $gatewayId, $companyId);
		#echo $stmt;
		if ($stmt->execute()){
			#echo "registro exitoso\n";
		}else{
			echo $conn->error . "\n";
		}

		$dataSetId = mysqli_insert_id($conn);

		//---inserting Values table
		if($ds_logType=="cumulative"){
			foreach ($ds_values as $value) {
				$stmt = $conn->prepare("INSERT INTO ".$_CONF['db_db'].".Values (value, DataSets_idDataSet , Companies_idCompany) VALUES ( ?, ?, ? )");
				$stmt->bind_param("sss", $value, $dataSetId, $companyId);
				#echo $stmt;
				if ($stmt->execute()){
					#echo "registro exitoso\n";
				}else{
					echo $conn->error . "\n";
				}
			}
		}else if($ds_logType=="linear"){
			$i=0;
			foreach ($ds_values as $value) {
				$stmt = $conn->prepare("INSERT INTO ".$_CONF['db_db'].".Values (value, timeStamp, DataSets_idDataSet , Companies_idCompany) VALUES ( ?, ?, ?, ? )");
				$stmt->bind_param("ssss", $value, $ds_timeStamps[$i], $dataSetId, $companyId);
				#echo $stmt;
				if ($stmt->execute()){
					#echo "registro exitoso\n";
					$i = $i+1;
				}else{
					echo $conn->error . "\n";
				}
			}			
		}

	}else{// Tag does not exist
		echo "error: Tag's TID not registered in the platfom \n";
	}
}else{ // Gateway does not exist
	echo "error: Gateway's serial not registered in the platform\n";
}
}else{// company not registered
	echo "error: Validating companyId\ngatewayCompanyId: ".$gatewayCompanyId."\ntagCompanyId: ".$tagCompanyId."\n";
}



?>
