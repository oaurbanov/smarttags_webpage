<?php
	include("config.php");
    // Create connection
    $conn = new mysqli($_CONF['db_host'],$_CONF['db_user'],$_CONF['db_pass'],$_CONF['db_db']);
    // Check connection
    if ($conn->connect_error) {
         die("Connection failed: " . $conn->connect_error);
    }

?>