<?php
session_start();
include("../mainInclude.php");

$gatewayId  = $_POST['gatewayId'];
$result_array = array();

/* SQL query to get results from database */
$queryReq = "SELECT idGateway, name, serial, location FROM Gateways WHERE idGateway = ".$gatewayId;
$result = $conn->query($queryReq);

/* If there are results from database push to result array */
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        array_push($result_array, $row);
    }
}

/* send a JSON encded array to client */
echo json_encode($result_array);

$conn->close();

?>