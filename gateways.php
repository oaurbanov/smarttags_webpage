<?php

    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
   
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Oscar Urbano">

<title>Orbis gateways</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">    

<style>

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #FFF; /*color of the x of the close button*/
    opacity: 0.7;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

</style>

</head>

<body onload="load()">
 
<?php 
    if(isset($_SESSION['user_rol']) && $_SESSION['user_rol'] == 'admin'){
        include('nav_admin.html');
    }
    else{
        include('nav_user.html');
    }
    include('php/mainInclude.php');//conection to dataBase
?>

<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center" colspan="6">Gateways</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="text-center"> ID </td>
            <th class="text-center">Name</td>
            <th class="text-center">Serial</td>
            <th class="text-center">Location</td>
            <th class="text-center">Last login</td>
            <td class="text-center">
            <?php
            if($_SESSION["user_rol"]=="admin"){
                echo '
                    <button class = "btn-primary btn-sm" onclick="onAdd()" style="width:auto;"> Add </button>
                ';
            }else{
                echo '
                    <button class = "btn-primary btn-sm" onclick="notAllowed()" style="width:auto;"> Add </button>
                ';
            }
            ?>
            </td>
        </tr>


        <?php        
        $result = $conn->query("SELECT * FROM Gateways WHERE Companies_idCompany = ".$_SESSION["company_id"]." ORDER BY idGateway desc;");
        while($row = $result->fetch_array()){ //When table empty: //Fatal error: Call to a member function fetch_array() on boolean in /opt/lampp/htdocs/smartTags/gateways.php on line 104
            if ( $row['idGateway']>1 && ($_SESSION["user_rol"]=="admin") ){ //preventing from modifying initial gateways
                echo "
                        <tr>
                            <td>".$row['idGateway']."</td>
                            <td>".$row['name']."</td>
                            <td>".$row['serial']."</td>
                            <td>".$row['location']."</td>
                            <td>".$row['lastLog']."</td>
                            <td class=\"text-center\">
                                <button class=\"btn-secondary btn-sm\" style=\"width:auto\" onclick= \"editGateway(".$row['idGateway'].")\" >
                                    Edit
                                </button> 
                            </td>
                        </tr>
                    ";
            }else{
                echo "
                        <tr>
                            <td>".$row['idGateway']."</td>
                            <td>".$row['name']."</td>
                            <td>".$row['serial']."</td>
                            <td>".$row['location']."</td>
                            <td>".$row['lastLog']."</td>
                            <td class=\"text-center\">
                                <button class=\"btn-secondary btn-sm\" style=\"width:auto\" onclick= \"notAllowed()\" >
                                    Edit
                                </button> 
                            </td>
                        </tr>
                    ";
            }
        } 
        ?>
<!--         <tr>
            <td>0002</td>
            <td><a href="#">Tarongers gate 1</a></td>
            <td>89430a2</td>
            <td> 45.9 , 25.5 </td>
            <td> 24-12-2017 12:00:32 </td>
            <td class="text-center"><button class = "btn-secondary btn-sm" style="width:auto;"> Edit </button></td>
        </tr>
        <tr>
            <td>0001</td>
            <td><a href="#">Mercadona kr 35</a></td>
            <td>89430b3</td>
            <td> 45.9 , 25.4 </td>
            <td> 24-12-2017 12:00:32 </td>
            <td class="text-center"><button class = "btn-secondary btn-sm" style="width:auto;"> Edit </button></td>
        </tr> -->
    </tbody>
</table>    
</div>  
</div>  
</div>


<!-- popup modal form addGatewayMenu -->
<div  id="addGatewayDiv" class="modal" >
<form class="modal-content animate" action="include/addGateway.php" method="POST">

<div class="imgcontainer" style="background-color: #336699; margin: 0px 0 0px 0; text-align: center; ">
    <img src="img/logov2.png" alt="orbis logo" style="height: 100px;">        
</div>

<span onclick="document.getElementById('addGatewayDiv').style.display='none'" class="close" title="Close Modal">&times; </span>

<div class="container-fluid">
    <fieldset class="form-group">
        <label for="name">Name</label>
        <input id="addGatewayMenu_name" type="text" name ="name" class="form-control">
    </fieldset>
    <fieldset class="form-group">
        <label for="serial">Serial</label>
        <input id="addGatewayMenu_serial" type="text" name ="serial" class="form-control">
    </fieldset>
    <fieldset class="form-group">
        <label for="location">Location</label>
        <input id="addGatewayMenu_location" type="text" name ="location" class="form-control">
    </fieldset>
    <div class="row">
        <button type="button" class="btn btn-success center-block" onclick="addGateway();">Add</button>
    </div>
    <br>
</div>

</form>
</div>


<!-- popup modal form editGatewayMenu -->
<div  id="editGatewayDiv" class="modal" >
<form class="modal-content animate" action="include/modifyGateway.php" method="POST">

<div class="imgcontainer" style="background-color: #336699; margin: 0px 0 0px 0; text-align: center; ">
    <img src="img/logov2.png" alt="orbis logo" style="height: 100px;">        
</div>

<span onclick="document.getElementById('editGatewayDiv').style.display='none'" class="close" title="Close Modal">&times; </span>

<div class="container-fluid">
    <fieldset class="form-group">
        <input id="editGatewayMenu_id" name ="gatewayId" type="hidden"></input>
        <label for="name">Name</label>
        <input id="editGatewayMenu_name" type="text" name ="name" class="form-control">
    </fieldset>
    <fieldset class="form-group">
        <label for="serial">Serial</label>
        <input id="editGatewayMenu_serial" type="text" name ="serial" class="form-control">
    </fieldset>
    <fieldset class="form-group">
        <label for="location">Location</label>
        <input id="editGatewayMenu_location" type="text" name ="location" class="form-control">
    </fieldset>
    <div class="row" style="text-align: center;">
        <div class="col-md-2">
            <!-- <br> -->
        </div>
        <div class="col-md-2" >
            <button type="button" onclick="modifyGateway();" class="btn btn-success center-block">modify</button>
        </div>
        <div class="col-md-4">
            <br>
        </div>
        <div class="col-md-2">
            <button type="button" onclick="removeGateway();" class="btn btn-danger center-block">remove</button>
        </div>
        <div class="col-md-2">
            <!-- <br> -->
        </div>
    </div>
    <br>
</div>

</form>
</div>
<!-- Example of popup form
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_login_form_modal -->



<script type="text/javascript"> 

function load() {
  
}

function onAdd(){
    document.getElementById('addGatewayDiv').style.display='block';
}

function notAllowed(){
    //alert("not allowed");
}

function addGateway(){
    var name = $('#addGatewayMenu_name').val();
    var serial = $('#addGatewayMenu_serial').val();
    var location = $('#addGatewayMenu_location').val();
    //alert("name: "+name+", serial: "+serial+" , location: "+location);

    if ( name.length > 0  && (serial.length == 8) && location.length > 0 ){ //preventing invalid fields in addGatewayMenu

        $.ajax({
            method:"POST",
            url:"php/gatewaysManagement/addGateway.php",
            data: {"name":name,"serial":serial,"location":location}
        }).done(function(data){
            if(data.length>0){
                alert(data);
                window.location="gateways.php"
            }
            else{
                $('#addGatewayDiv').hide();
                window.location="gateways.php"
            }
        });

    }else{
        var msj = "";
        if(name.length==0){
            msj += "name is empty\n";
        }
        if(location.length==0){
            msj += "location is empty\n";
        }
        if(serial.length != 8){
            msj += "serial has to be 8 char long";
        }
        alert(msj);
    }

}




function editGateway(idGateway){
    //alert("I'm gonna edit Gateway: " +idGateway);
    var id = 0;
    var name = '';
    var serial = '';
    var location = '';

    //getting Data from DataBase with the php script
    $.ajax({
        method:"POST",
        url:"php/gatewaysManagement/getDataGatewayFromId.php",
        data: {"gatewayId":idGateway}
    }).done(function(data){
        //console.log(data);
        var result = $.parseJSON(data);
        console.log(result);
        id = result[0].idGateway
        name = result[0].name;
        serial = result[0].serial;
        location = result[0].location;


    //Updating fields of the popUpMenu
        $(document.getElementById('editGatewayDiv').style.display='block').ready(function(){
            $('#editGatewayMenu_id').val(id);
            $('#editGatewayMenu_name').val(name);
            $('#editGatewayMenu_serial').val(serial);
            $('#editGatewayMenu_location').val(location);
        });

    });


}


function modifyGateway(){
    var idGateway = $('#editGatewayMenu_id').val();
    var name = $('#editGatewayMenu_name').val();
    var serial = $('#editGatewayMenu_serial').val();
    var location = $('#editGatewayMenu_location').val();
    //alert("gatewayId: "+idGateway+", name: "+name+", serial: "+serial+" , location: "+location);

    if ( name.length > 0  && serial.length > 0 && location.length > 0 ){ //preventing invalid fields in editGatewayMenu
        $.ajax({
            method:"POST",
            url:"php/gatewaysManagement/modifyGateway.php",
            data: {"gatewayId": idGateway,"name":name,"serial":serial,"location":location}
        }).done(function(data){
            if(data.length>0){
                alert(data);
                window.location="gateways.php"
            }
            else{
                $('#addGatewayDiv').hide();
                window.location="gateways.php"
            }
        });
    }else{
        alert("name, serial or location are empty");
    }
}


function removeGateway(){
    var idGateway = $('#editGatewayMenu_id').val();
    //alert("I'm gonna remove gateway: "+idGateway);

    $.ajax({
        method:'POST',
        url:"php/gatewaysManagement/removeGateway.php",
        data:{"gatewayId":idGateway}
    }).done(function(data){
        if(data.length>0) alert(data);
        $('#editGatewayDiv').hide();
        window.location="gateways.php";
    });
}

</script>
    
    
  

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>   
</html>
