<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
   
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Oscar Urbano">

<title>Orbis homepage</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">    

<style type="text/css">
/*.centerBlock {
  display: table;
  margin: auto;
}*/
</style>

</head>

<body onload="load()">
 
    <?php 
        if(isset($_SESSION['user_rol']) && $_SESSION['user_rol'] == 'admin'){
            include('nav_admin.html');
        }
        else{
            include('nav_user.html');
        }
        include('php/mainInclude.php');//conection to dataBase
     ?>


    <div class="row" style="text-align:center;"> <!-- //sensors row -->
        
        <!-- temperature -->
        <div class="col-md-3 col-md-offset-3" style="text-align: center">

            <div class="form-group">
                <br>
                <button type="button" class="btn btn-primary" >Temperature</button>
            </div>

            <div class="form-group">
                <img src="img/placeholder_temp.jpeg" alt="temp icon" height="60">
                <div id="valTemp"></div>
            </div>

            <div class="form-group">
                <select id="senT" name="sensoresT" onchange="buildDataSetsSelector( $('#senT option:selected') );" >
                    <option value=0 selected disabled>Select one tag</option>
                    <!-- <option value=1> E0045002238001 </option>
                    <option value=2> E0045002238002 </option>
                    <option value=3> E0045002238003 </option> -->
                </select>
            </div>

        </div> 

        <!-- humidity -->
        <div class="col-md-3" style="text-align: center">

            <div class="form-group">
                <br>
                <button type="button" class="btn btn-primary" >Humidity</button>
            </div>

            <div class="form-group">
                <img src="img/placeholder_water.png" alt="humidity icon" height="60">
                <div id="valHum"></div>
            </div>

            <div class="form-group">
                <select id="senH" name="sensoresH" onchange="buildDataSetsSelector( $('#senH option:selected') );" >
                    <option value=0 selected disabled>Select one tag</option>
                    <!-- <option value=1> E0045002238001 </option>
                    <option value=2> E0045002238002 </option>
                    <option value=3> E0045002238003 </option> -->
                </select>
            </div>
        </div> 

    </div>


    <div class="row" style="text-align:center;">
    <div class="col-md-3 col-md-offset-3">
        <select id="tempDataSets" onchange="buildChart( $('#tempDataSets option:selected') );" >
            <option value=0 selected disabled> Select one DataSet</option>            
        </select>
    </div>        
    <div class="col-md-3">
        <select id="humDataSets" onchange="buildChart( $('#humDataSets option:selected') );">
            <option value=0 selected disabled> Select one DataSet </option>            
        </select>
    </div>        
    </div>

    <br>

    <div class="row" style="text-align:center;">
        <div class="col-md-10 col-md-offset-1" style="text-align: center;">
            <div class="center-block" style="width: 1200px">
                <canvas id="stChart"  ></canvas>                    
            </div>
        </div>
    </div>

    <br>

    <div id="divDownloadCSV" hidden="hidden" class="row">
        <button  type="submit" class="btn btn-success center-block" onclick="downloadCSV()">Downlad CSV</button>
    </div>
    
    <br>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="js/main.js" ></script>


<script type="text/javascript">
    
function load(){
    buildTagsSelector("temperature");
    buildTagsSelector("humidity");
    //getHumTags();
}

function buildTagsSelector(sensorType){
    var TIDs_array = new Array();

    //querying tags_TID where companyId and sensorType
    $.ajax({
        method : "POST",
        url : "php/homeManagement/getTagDataFromSensorType.php",
        data : {"sensorType" : sensorType}

    }).done(function(data){
        //console.log(data);
        result = $.parseJSON(data);
        //console.log(result);

        //building tags selector
        $.each(result, function(i, item){
            if (TIDs_array.indexOf(item.TID) != -1){//check if TID is already appended
                //console.log("repeated TID: "+item.TID);
            }else{
                if (sensorType=="temperature"){
                    $('#senT').append($('<option>',{
                        value: i+1,
                        text : item.TID,
                        id : item.idTag,
                        sensorType : sensorType,
                        companyId : item.Companies_idCompany
                    }));                
                }
                if (sensorType=="humidity"){
                    $('#senH').append($('<option>',{
                        value: i+1,
                        text : item.TID,
                        id : item.idTag,
                        sensorType : sensorType,
                        companyId : item.Companies_idCompany
                    }));                
                }
                TIDs_array.push(item.TID);//preventing repeated TID
            }        
        });

    });

    delete TIDs_array;
}


function buildDataSetsSelector(tag){
    //console.log(  $('select#senT option:selected').attr('sensorType')  );

    var tagId = tag.attr('id');
    var companyId = tag.attr('companyId');
    var sensorType = tag.attr('sensorType');

    //querying DataSets where tagId , companyId and sensorType
    $.ajax({
        method: "POST",
        url : "php/homeManagement/getDataSetsDataFromTag.php",
        data : { "tagId":tagId , "companyId":companyId, "sensorType": sensorType}
    }).done(function(data){
        //console.log(data);
        result = $.parseJSON(data);
        //console.log(result);

        //empty defined dataSetSelector to prevent reinsertion
        if (sensorType=="temperature"){
            //$('#tempDataSets').empty();//not needed
            $('#tempDataSets').html("<option value=0 selected disabled>Select one DataSet</option>");

        }
        if (sensorType=="humidity"){
            //$('#humDataSets').empty();//not needed
            $('#humDataSets').html("<option value=0 selected disabled>Select one DataSet</option>");
        }

        //building dataSets selector
        $.each(result, function(i, item){
            if (sensorType=="temperature"){
                $('#tempDataSets').append($('<option>',{
                    value: i+1,
                    text : item.timeStamp,
                    timeStamp : item.timeStamp,
                    id : item.idDataSet,
                    sensorType : sensorType
                }));                
            }
            if (sensorType=="humidity"){
                $('#humDataSets').append($('<option>',{
                    value: i+1,
                    text : item.timeStamp,
                    timeStamp : item.timeStamp,
                    id : item.idDataSet,
                    sensorType : sensorType
                }));                
            }
        });

    });
}

function buildChart(dataSet){
    //console.log(dataSet);

    //empty the other dataSetSelector
    var sensorType = dataSet.attr('sensorType');
    if (sensorType=="temperature"){
        $('#humDataSets').val(0);
    }
    if (sensorType=="humidity"){
        $('#tempDataSets').val(0);
    }
    
    var dataSetId = dataSet.attr('id');
    var dataSetTimeStamp = dataSet.attr('timeStamp');
    //console.log(" dataSetId: "+dataSetId+", dataSetTimeStamp: "+dataSetTimeStamp);
    prepareChart(dataSetId);
    $('#divDownloadCSV').show();
}

</script>    



</body>   
</html>
