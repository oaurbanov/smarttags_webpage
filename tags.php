<?php

//TODO:
// Add sensor type and value from another table

	session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
   
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Oscar Urbano">

<title>Orbis tags</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">    

<style>

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #FFF; /*color of the x of the close button*/
    opacity: 0.7;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

</style>

</head>

<body onload="load()">
 
<?php 
    if(isset($_SESSION['user_rol']) && $_SESSION['user_rol'] == 'admin'){
        include('nav_admin.html');
    }
    else{
        include('nav_user.html');
    }
    include('php/mainInclude.php');//conection to dataBase
?>

<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center" colspan="7">Tags</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="text-center"> ID </td>
            <th class="text-center"> TID </td>
            <th class="text-center"> EPC </td>
            <th class="text-center">Last reading</td>
            <th class="text-center">Last gateway ID</td>
            <td class="text-center">
            <?php
            if($_SESSION["user_rol"]=="admin"){
                echo '
                    <button class = "btn-primary btn-sm" onclick="onAdd()" style="width:auto;"> Add </button>
                ';
            }else{
                echo '
                    <button class = "btn-primary btn-sm" onclick="notAllowed()" style="width:auto;"> Add </button>
                ';
            }
            ?>
            </td>
        </tr>

        <?php        
        $execItems = $conn->query("SELECT * FROM Tags WHERE Companies_idCompany = ".$_SESSION["company_id"]."  ORDER BY idTag desc;");
        while($infoItems = $execItems->fetch_array()){
            if ($infoItems['idTag']>2 && ($_SESSION["user_rol"]=="admin") ){ //preventing from modifying initial tags
                echo "
                        <tr>
                            <td>".$infoItems['idTag']."</td>
                            <td>".$infoItems['TID']."</td>
                            <td>".$infoItems['EPC']."</td>
                            <td>".$infoItems['lastRead']."</td>
                            <td>".$infoItems['Gateways_idGateway']."</td>
                            <td class=\"text-center\">
                                <button class=\"btn-warning btn-sm\" style=\"width:auto\" onclick= \"removeTag(".$infoItems['idTag'].")\" >
                                    Remove
                                </button> 
                            </td>
                        </tr>
                    ";
            }else{
                echo "
                        <tr>
                            <td>".$infoItems['idTag']."</td>
                            <td>".$infoItems['TID']."</td>
                            <td>".$infoItems['EPC']."</td>
                            <td>".$infoItems['lastRead']."</td>
                            <td>".$infoItems['Gateways_idGateway']."</td>
                            <td class=\"text-center\">
                                <button class=\"btn-warning btn-sm\" style=\"width:auto\" onclick= \"notAllowed();\" >
                                    Remove
                                </button> 
                            </td>
                        </tr>
                    ";                
            }
        } 
        ?>

<!--         <tr>
            <td><a href="#">E0045002238002</a></td>
            <td><a href="#">F0045002238002</a></td>
            <td> 24-12-2017 12:00:32 </td>
            <td> 001 </td>
            <td class="text-center"><button class = "btn-warning btn-sm" style="width:auto;"> Remove </button></td>
        </tr>
        <tr>
            <td><a href="#">E0045002238003</a></td>
            <td><a href="#">F0045002238003</a></td>
            <td> 24-12-2017 12:20:34 </td>
            <td> 001 </td>
            <td class="text-center"><button class = "btn-warning btn-sm" style="width:auto;"> Remove </button></td>
        </tr> -->
    </tbody>
</table>    
</div>  
</div>  
</div>


<!-- popup modal form addTagMenu -->
<div  id="addTagDiv" class="modal" >
<form class="modal-content animate" action="include/addTag.php" method="POST">

<div class="imgcontainer" style="background-color: #336699; margin: 0px 0 0px 0; text-align: center; ">
    <img src="img/logov2.png" alt="orbis logo" style="height: 100px;">        
</div>

<span onclick="document.getElementById('addTagDiv').style.display='none'" class="close" title="Close Modal">&times; </span>

<div class="container-fluid">
    <fieldset class="form-group">
        <label for="user">TID</label>
        <input id="addTagMenu_TID" type="text" name ="TID" class="form-control">
    </fieldset>
    <fieldset class="form-group">
        <label for="user">EPC</label>
        <input id="addTagMenu_EPC" type="text" name ="EPC" class="form-control">
    </fieldset>
    <div class="row">
        <button type="button" class="btn btn-success center-block" onclick="addTag();" >Add</button>
    </div>
    <br>
</div>

</form>
</div>
<!-- Example of popup form
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_login_form_modal -->






<script type="text/javascript"> 

function load() {
  
}

function onAdd(){
    document.getElementById('addTagDiv').style.display='block'; 
}

function notAllowed(){
    //alert("not allowed");
}

function addTag(){
    const TAG_ID_SIZE = 12*2 ;//TID EPC size
    var TID = $('#addTagMenu_TID').val();
    var EPC = $('#addTagMenu_EPC').val();
    //alert("TID: "+TID+", EPC: "+EPC);

    if ( (TID.length==TAG_ID_SIZE && isAllHexa(TID)) && (EPC.length==TAG_ID_SIZE && isAllHexa(EPC)) ){//preventing wrong entry values

        $.ajax({
            method:"POST",
            url:"php/tagsManagement/addTag.php",
            data: {"TID":TID,"EPC":EPC}
        }).done(function(data){
            if(data.length>0){
                alert(data);
                window.location="tags.php"
            }
            else{
                $('#addTagDiv').hide();
                window.location="tags.php"
            }
        });

    }else{
        var msj = ""
        if( TID.length!=TAG_ID_SIZE ){
            msj += "TID must be "+TAG_ID_SIZE+" Characters long\n";
        }
        if( !isAllHexa(TID) ){
            msj += "TID must have only Hexadecimal values\n";
        }
        if( EPC.length!=TAG_ID_SIZE ){
            msj += "EPC must be "+TAG_ID_SIZE+" Chararacters long\n";
        }
        if( !isAllHexa(EPC) ){
            msj += "EPC must have only Hexadecimal values\n";
        }
        alert(msj);
    }

}

function removeTag(idTag){
    //alert("I'm gonna remove tag: "+idTag);
    $.ajax({
        method:'POST',
        url:"php/tagsManagement/removeTag.php",
        data:{"tagId":idTag}
    }).done(function(data){
        if(data.length>0) alert(data);
        window.location="tags.php"
    });
}

function isHexa (myChar = "7" ){
    var hexaStr ="0123456789ABCDEF";
    var eval = false;
    for (var i = 0; i < hexaStr.length ; i++) {
        if (myChar == hexaStr[i]) eval = true; 
    }
    return eval;
}

function isAllHexa(str="0123456789ABCDEF"){
    for (var i = 0; i < str.length ; i++) {
        if (!isHexa(str[i])) return false; 
    }
    return true;
}

</script>
    
    
  

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>   
</html>
