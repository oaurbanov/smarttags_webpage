<html lang="en-US">

<head>
   
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Oscar Urbano">

<title>Orbis Login page</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">    

</head>

<body>

<div style="background-color: #336699;">
    <img src="img/logov2.png" alt="orbis logo" style="height: 100px; margin-bottom: -15px" class="center-block">
    <h2 style="color:#fff; height: 45px; text-align: center; font-style: italic;"> smart Tags </h2>
</div>

<div id="page-content-wrapper">
	<div class="container-fluid">
		<div class="container" style="width: 500px">
			<form action="php/startSession.php" method="POST">
				<fieldset class="form-group">
					<label for="user">User</label>
					<input type="text" name ="user" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="pass">Password</label>
					<input type="password" name="pass" class="form-control">
				</fieldset>
					<div class="row">
						<div class="col-md-4 col-md-offset4 center-block">
							 <button type="submit" class="btn btn-primary center-block">Login</button>
						</div>
						<div class="col-md-8 col-md-offset4 center-block" style="vertical-align: center">
							 <!-- <button type="submit" class="btn btn-primary center-block">Login</button> -->
							<?php
								session_start();
								if(isset($_SESSION['correct_login']) && $_SESSION['correct_login'] == 'no'){
									echo '<p class="text-danger"> Incorrect user or password </p>';
									$_SESSION['correct_login'] = 'yes';//reseting variable
								}
							?>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>
</html>