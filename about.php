<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
   
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Oscar Urbano">

<title>Orbis about</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">    

</head>

<body>
 
<?php 
    if(isset($_SESSION['user_rol']) && $_SESSION['user_rol'] == 'admin'){
        include('nav_admin.html');
    }
    else{
        include('nav_user.html');
    }
    include('php/mainInclude.php');//conection to dataBase
?>

<div style="background-color: #fff;">
    <img src="img/logo.jpg" alt="orbis logo" style="height: 200px; margin-bottom: -15px" class="center-block">
    <h2 style="color:#336699; height: 45px; text-align: center; font-style: italic;"> smart Tags </h2>
</div>


<div class="container">
<div class="row">
<div class="col-md-6 col-md-offset-3">
<p align="justify">	
        <i><b>Smart Tags</b></i> is a spin-off of the <a target="_blank" href="http://168.176.26.28/sensores_info/"> Orbis IoT platform </a> developed by the <a target="_blank" href="https://es.wikipedia.org/wiki/EMC-UN">EMC-UN </a> research group at the Universidad Nacional de Colombia.
        

        <br><br>

        This project is being developed at the RIS research group in the Universitat Politècnica de València and applies RFID technology into its devices to produce:
		chip, small and low-power sensors, applied to end-less applications, like cold-chain monitoring, transportation monitoring and intelligent agriculture.

		<br><br>

		<b> Wiki: </b> 
		<a target="_blank" href="https://sites.google.com/site/rfid2iot/home">https://sites.google.com/site/rfid2iot/home</a>
</p>
</div>
</div>
</div>

    
    
  

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>   
</html>
