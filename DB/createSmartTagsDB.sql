SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `smartTags` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `smartTags` ;

-- -----------------------------------------------------
-- Table `smartTags`.`Companies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smartTags`.`Companies` (
  `idCompany` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCompany`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `idCompany_UNIQUE` (`idCompany` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smartTags`.`Gateways`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smartTags`.`Gateways` (
  `idGateway` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `serial` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NULL,
  `lastLog` TIMESTAMP NULL,
  `Companies_idCompany` INT NOT NULL,
  PRIMARY KEY (`idGateway`, `Companies_idCompany`),
  UNIQUE INDEX `idGateway_UNIQUE` (`idGateway` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `serial_UNIQUE` (`serial` ASC),
  INDEX `fk_Gateways_Companies1_idx` (`Companies_idCompany` ASC),
  CONSTRAINT `fk_Gateways_Companies1`
    FOREIGN KEY (`Companies_idCompany`)
    REFERENCES `smartTags`.`Companies` (`idCompany`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smartTags`.`Tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smartTags`.`Tags` (
  `idTag` INT NOT NULL AUTO_INCREMENT,
  `TID` VARCHAR(45) NOT NULL,
  `EPC` VARCHAR(45) NULL,
  `lastRead` TIMESTAMP NULL,
  `Gateways_idGateway` INT NOT NULL,
  `Companies_idCompany` INT NOT NULL,
  PRIMARY KEY (`idTag`, `Companies_idCompany`),
  UNIQUE INDEX `TID_UNIQUE` (`TID` ASC),
  UNIQUE INDEX `idTag_UNIQUE` (`idTag` ASC),
  INDEX `fk_Tags_Gateways1_idx` (`Gateways_idGateway` ASC),
  INDEX `fk_Tags_Companies1_idx` (`Companies_idCompany` ASC),
  CONSTRAINT `fk_Tags_Gateways1`
    FOREIGN KEY (`Gateways_idGateway`)
    REFERENCES `smartTags`.`Gateways` (`idGateway`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tags_Companies1`
    FOREIGN KEY (`Companies_idCompany`)
    REFERENCES `smartTags`.`Companies` (`idCompany`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smartTags`.`DataSets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smartTags`.`DataSets` (
  `idDataSet` INT NOT NULL AUTO_INCREMENT,
  `sensorType` VARCHAR(45) NOT NULL,
  `logType` VARCHAR(45) NOT NULL,
  `ttLog` INT NOT NULL,
  `timeStamp` VARCHAR(45) NOT NULL,
  `Tags_idTag` INT NOT NULL,
  `Gateways_idGateway` INT NOT NULL,
  `Companies_idCompany` INT NOT NULL,
  PRIMARY KEY (`idDataSet`, `Tags_idTag`, `Companies_idCompany`),
  UNIQUE INDEX `idMeasure_UNIQUE` (`idDataSet` ASC),
  INDEX `fk_DataSets_Tags1_idx` (`Tags_idTag` ASC),
  INDEX `fk_DataSets_Gateways1_idx` (`Gateways_idGateway` ASC),
  INDEX `fk_DataSets_Companies1_idx` (`Companies_idCompany` ASC),
  CONSTRAINT `fk_DataSets_Tags1`
    FOREIGN KEY (`Tags_idTag`)
    REFERENCES `smartTags`.`Tags` (`idTag`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DataSets_Gateways1`
    FOREIGN KEY (`Gateways_idGateway`)
    REFERENCES `smartTags`.`Gateways` (`idGateway`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DataSets_Companies1`
    FOREIGN KEY (`Companies_idCompany`)
    REFERENCES `smartTags`.`Companies` (`idCompany`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smartTags`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smartTags`.`Users` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `rol` VARCHAR(45) NOT NULL,
  `lastLog` TIMESTAMP NULL,
  `Companies_idCompany` INT NOT NULL,
  PRIMARY KEY (`idUser`, `Companies_idCompany`),
  UNIQUE INDEX `idUser_UNIQUE` (`idUser` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `fk_Users_Companies1_idx` (`Companies_idCompany` ASC),
  CONSTRAINT `fk_Users_Companies1`
    FOREIGN KEY (`Companies_idCompany`)
    REFERENCES `smartTags`.`Companies` (`idCompany`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smartTags`.`Values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smartTags`.`Values` (
  `idValue` INT NOT NULL AUTO_INCREMENT,
  `value` FLOAT NULL,
  `timeStamp` TIMESTAMP NULL,
  `DataSets_idDataSet` INT NOT NULL,
  `Companies_idCompany` INT NOT NULL,
  PRIMARY KEY (`idValue`, `DataSets_idDataSet`, `Companies_idCompany`),
  UNIQUE INDEX `idValue_UNIQUE` (`idValue` ASC),
  INDEX `fk_Values_DataSets_idx` (`DataSets_idDataSet` ASC),
  INDEX `fk_Values_Companies1_idx` (`Companies_idCompany` ASC),
  CONSTRAINT `fk_Values_DataSets`
    FOREIGN KEY (`DataSets_idDataSet`)
    REFERENCES `smartTags`.`DataSets` (`idDataSet`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Values_Companies1`
    FOREIGN KEY (`Companies_idCompany`)
    REFERENCES `smartTags`.`Companies` (`idCompany`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
