//TODO:
// Support for ds_logType == linear 
// Memory mapping changes on ds_sensorType "temperature" and "humidity"

const dataSize = parseInt( (30 + 10 )/0.2 +1 +2);

// //if  logType, ttLog , sensorType, timeStamp

// //if(logType=="cumulative")

// //if(logType=="linear") //TODO:


function chart(){

}


function prepareChart(dataSetId){
    var dataSet;
    var values;

    //Get DataSet Data where dataSetId
    $.ajax({
        method: "POST",
        url: "php/homeManagement/getDataSetDataFromId.php",
        data: {"dataSetId":dataSetId}
    }).done(function(data){
        //console.log(data);
        dataSet = $.parseJSON(data);
        //console.log(dataSet);

        //Get Values Data where dataSetId
        $.ajax({
            method:"POST",
            url : "php/homeManagement/getValuesWhereDataSetId.php",
            data: {"dataSetId":dataSetId}
        }).done(function(data){
            //console.log(data);
            values = $.parseJSON(data);
            //console.log(values);

            organizeData(dataSet, values);    
        });
    });

}

function organizeData(dataSet, values){
    ds_logType =  dataSet[0]["logType"] ;
    ds_ttLog =  dataSet[0]["ttLog"] ;
    ds_sensorType =  dataSet[0]["sensorType"] ;
    ds_timeStamp =  dataSet[0]["timeStamp"] ;
    //console.log(" ds_logType: "+ds_logType+" , ds_ttLog: "+ds_ttLog+", ds_sensorType: "+ds_sensorType+", ds_timeStamp:" +ds_timeStamp);

    if(ds_logType=="cumulative"){
        if(ds_sensorType=="temperature"){
            //data for y-axis
            var values_array = new Array();
            values_array.push( Math.round(values[values.length-2]["value"] * (ds_ttLog/60)*100)/100);//organizing the last  two values
            for (var i = 0; i <values.length-2; i++) {
                values_array.push( Math.round(values[i]["value"] * (ds_ttLog/60)*100)/100);//values in hours and two decimals
            }
            values_array.push( Math.round(values[values.length-1]["value"] * (ds_ttLog/60)*100)/100);//organizing the last  two values

            //data for x-axis
            var xAxis = new Array();
            xAxis.push('<-10');//TODO:
            var minValue = -10.0 ; var stepSize = 0.2;//TODO:should be included in dataSet?
            for (var i = 0; i < dataSize-2; i++) {
                 var t = minValue + i*stepSize;
                 t = Math.round(t*10)/10; //rounded to 1 decimal
                 xAxis.push(t);
                 //console.log(t)
            }
            xAxis.push('>30');//TODO:

            var totalTime = 0.0;
            for (var i = 0; i <values_array.length; i++) {
                totalTime += values_array[i];//total time in hours
            }
            var strDataTitle = "Total time: "+Math.round(totalTime)+" hours";
            properties = {"chartType": "bar", "chartFill":true, "title": "Integral Temperature", "dataTitle":strDataTitle , "xName": "Temperature [ ºC ]", "yName": "Time [ hours ]", "timeStamp":ds_timeStamp};
            createChart(xAxis, values_array, properties);
        }
        else if(ds_sensorType=="humidity"){
            //data for y-axis
            var values_array = new Array();
            values_array.push( Math.round(values[values.length-2]["value"] * (ds_ttLog/60)*100)/100);//organizing the last  two values
            for (var i = 0; i <values.length-2; i++) {
                values_array.push( Math.round(values[i]["value"] * (ds_ttLog/60)*100)/100);//values in hours and two decimals
            }
            values_array.push( Math.round(values[values.length-1]["value"] * (ds_ttLog/60)*100)/100);//organizing the last  two values

            //data for x-axis
            var xAxis = new Array();
            var minValue = 0.0 ; var stepSize = 0.5;//TODO:should be included in dataSet?
            for (var i = 0; i < dataSize-2; i++) {
                 var t = minValue + i*stepSize;
                 t = Math.round(t*10)/10; //rounded to 1 decimal
                 xAxis.push(t);
                 //console.log(t)
            }

            var totalTime = 0.0;
            for (var i = 0; i <values_array.length; i++) {
                totalTime += values_array[i];//total time in hours
            }
            var strDataTitle = "Total time: "+Math.round(totalTime)+" hours";
            properties = {"chartType": "bar", "chartFill":true, "title": "Integral Humidity", "dataTitle":strDataTitle , "xName": "Humidity [ % ]", "yName": "Time [ hours ]", "timeStamp":ds_timeStamp};
            createChart(xAxis, values_array, properties);
        }
    }
    else if(ds_logType=="linear"){
        if(ds_sensorType=="temperature"){            
            var values_array = new Array();//data for y-axis            
            var xAxis = new Array();//data for x-axis
            for (var i = 0; i <values.length; i++) {
                values_array.push( values[i]["value"] );
                xAxis.push( values[i]["timeStamp"] );
            }
            properties = {"chartType": "line", "chartFill":false, "title": "Linear Temperature", "dataTitle":" Temperature ", "xName": " Time ", "yName": "Temperature [ ºC ]", "timeStamp":ds_timeStamp};
            createChart(xAxis, values_array, properties);
        }
        else if(ds_sensorType=="humidity"){            
            var values_array = new Array();//data for y-axis            
            var xAxis = new Array();//data for x-axis
            for (var i = 0; i <values.length; i++) {
                values_array.push( values[i]["value"] );
                xAxis.push( values[i]["timeStamp"] );
            }
            properties = {"chartType": "line", "chartFill":false, "title": "Linear Humidity", "dataTitle":" Humidity ", "xName": " Time ", "yName": "Humidity[ % ]", "timeStamp":ds_timeStamp};
            createChart(xAxis, values_array, properties);
        }
    }

}
    

var stChart, stChartCSV;
function createChart(xAxis, yAxis, properties){

    const CHART = document.getElementById("stChart");
    //console.log(CHART);
    if (stChart) stChart.destroy(); //preventing error on hoover
    stChart = new Chart(CHART,{
        type : properties["chartType"],
        data: {
            labels: xAxis,
            datasets: [{
                label: properties["dataTitle"],
                fill: properties["chartFill"],
                backgroundColor: 'rgb(221, 127, 59)',// 'Chocolate',//'rgb(51, 102, 153)',
                borderColor: 'rgb(158, 93, 47)',//'Chocolate',//'rgb(51, 102, 153)',
                data: yAxis
            }]
        },

        options: {
            responsive: true,
            title:{
                display:true,
                text:properties["title"]
            },
            tooltips: {
                        mode: 'index',
                        intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display:true,
                    scaleLabel: {
                        display: true,
                        labelString: properties["xName"]
                    }
                }],
                yAxes: [{
                    display:true,
                    scaleLabel: {
                        display:true,
                        labelString: properties["yName"]
                    }
                }]
            }
        }
    });
    stChartCSV = {"xAxis": xAxis, "yAxis": yAxis, "properties": properties}
}

function downloadCSV(){
    //console.log(stChartCSV);

    var x = stChartCSV["xAxis"];
    var y = stChartCSV["yAxis"];
    var properties = stChartCSV["properties"];

    var str = "Graph type: "+properties["title"] +" , "+properties["dataTitle"] +" , "+" timeStamp: "+properties["timeStamp"] +" \n";
    str += properties["xName"]+" , "+properties["yName"] + " \n" ;

    for (var i= 0; i < x.length; i++){
        str += x[i]+" , "+y[i] + " \n" ;
    }

    download(properties["timeStamp"]+'.csv', str);
}

//src: https://stackoverflow.com/questions/2897619/using-html5-javascript-to-generate-and-save-a-file
function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}


// createDefaultChart();
// function createDefaultChart(){
//         const CHART = document.getElementById("stChart");
//     //console.log(CHART);
//     let lineChart = new Chart(CHART,{
//         type : 'line',
//         data: {
//             labels: ["January", "February", "March", "April", "May", "June", "July"],
//             datasets: [{
//                 label: "Total Time",
//                 fill: true,
//                 backgroundColor: 'rgb(221, 127, 59)',// 'Chocolate',//'rgb(51, 102, 153)',
//                 borderColor: 'rgb(158, 93, 47)',//'Chocolate',//'rgb(51, 102, 153)',
//                 data: [0, 5, 0, 2, 20, 30, 10]
//             }]
//         },

//         options: {
//             responsive: true,
//             title:{
//                 display:true,
//                 text:'Integral Temperature'
//             },
//             tooltips: {
//                         mode: 'index',
//                         intersect: false,
//             },
//             hover: {
//                 mode: 'nearest',
//                 intersect: true
//             },
//             scales: {
//                 xAxes: [{
//                     display:true,
//                     scaleLabel: {
//                         display: true,
//                         labelString: "Temperature [ ºC ]"
//                     }
//                 }],
//                 yAxes: [{
//                     display:true,
//                     scaleLabel: {
//                         display:true,
//                         labelString: 'Time [ hours ]'
//                     }
//                 }]
//             }
//         }
//     });
// }