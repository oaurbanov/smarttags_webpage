# Smart Tags #

Welcome to the SmartTags webPage repository.  
Wanna know what the project is about? Check out the [wiki](https://sites.google.com/site/rfid2iot/home).  
Here is the documentation of the [webPage](https://sites.google.com/site/rfid2iot/home/server-side).  

---

## Getting Started ##

1. Install a local web server, like LAMP or XAMPP
2. Copy the content into the /htdocs folder
3. Execute the PHPmyAdmin_smartTags.sql file to create the data base filled with some data.  
4. If you prefer to insert your own data you can use the createSmartTagsDB.sql to build the dataBase, start by adding a company and an admin user
5. Now you are ready to start uploading data! Just follow the jsonFormats.txt from the docs folder. You can login as username: admin , password: admin.

---

## TODO: ##

1. Support for deleting and managing DataSets.
2. Foreing keys error handling. Cannot delete tags or gateways associated to a determined dataSet, must first delete dataSets.

---


### Who do I talk to? ###

**Oscar Urbano** 	*oaurbanov@unal.edu.co*  

